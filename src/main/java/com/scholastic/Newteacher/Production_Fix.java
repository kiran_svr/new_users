package com.scholastic.Newteacher;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class Production_Fix {
	public static void main(String[] args) {

		WebDriver driver = null;
		FileInputStream fileInput = null;
		PrintWriter pw = null;

		try {
			File file = new File("ProductionEmails.txt");
			fileInput = new FileInputStream(file);

			pw = new PrintWriter("ProdResults" + System.currentTimeMillis() + ".txt");

			String emailid = null;
			BufferedReader br = new BufferedReader(new InputStreamReader(fileInput));
			while ((emailid = br.readLine()) != null) {
				try {
					driver = Browser_utl.getSpecificDriver("chrome");

					driver.get("https://storefront:cliff0rd@clubs-shingle.scholastic.com/home");
                    
					Thread.sleep(2000);
					
					driver.findElement(By.xpath(".//*[contains(@id,'dwfrm_login_username')]")).sendKeys(emailid.trim());
					driver.findElement(By.xpath(".//*[contains(@id,'dwfrm_login_password')]")).sendKeys("password1");
					driver.findElement(By.xpath(".//*[@id='dwfrm_login']/fieldset/div[1]/button")).click();

					Thread.sleep(2000);
					String completeRegPopup = driver.findElement(By.xpath(".//*[@id='profileSelection']/div/button"))
							.getText();
					// Complete Registration pop up

					pw.write(emailid + "\t");
					if (completeRegPopup != null) {

						pw.write(completeRegPopup + "\t" + "\n");
						// Complete Registration pop up
						driver.findElement(By.xpath(".//*[@id='profileSelection']/div/button")).click();
						Thread.sleep(1000);
						driver.findElement(By.xpath(".//*[@id='MyRoleForm']/div[1]/div[1]/div[1]")).click();
						Thread.sleep(1000);
			     	    driver.findElement(By.xpath(".//*[@id='scrollbar-0']/ul/li[3]/span")).click();
			         	Thread.sleep(1000);
			     	    driver.findElement(By.xpath(".//*[@id='MyRoleForm']/div[3]/div[1]/div[1]")).click();
			     	    Thread.sleep(1000);
			     	
			    	    driver.findElement(By.xpath(".//*[@id='scrollbar-2']/ul/li[2]/span[1]")).click();
			     	    Thread.sleep(1000);
			 
						 driver.findElement(By.xpath(".//*[@id='MyRoleForm']/div[3]/div[1]/div[2]/div[2]/input[2]")).click(); 
						 Thread.sleep(1000);
						 driver.findElement(By.xpath(".//*[@id='MyRoleForm']/div[8]/button[2]")).click();
						 Thread.sleep(1000);
						 driver.findElement(By.xpath(".//*[@id='dwfrm_multigrade_preschool']")).sendKeys("3");
						 Thread.sleep(1000);
						 driver.findElement(By.xpath(".//*[@id='MultiGradeForm']/div[5]/button[2]")).click(); 
						 
						 Thread.sleep(1000);
						 driver.findElement(By.xpath(".//*[@id='profileSelection']/div[1]/div[1]/div[1]")).click();
						 Thread.sleep(1000);
				         driver.findElement(By.xpath(".//*[@id='scrollbar-0']/ul/li[2]/span")).click();
				         Thread.sleep(1000);
				         
						 driver.findElement(By.xpath(".//*[@id='dwfrm_profileselection_teaching__firstyear-1']")).click(); 
						 Thread.sleep(1000);
						 driver.findElement(By.xpath(".//*[@id='dwfrm_profileselection_readingclub-1']")).click(); 
						 Thread.sleep(1000);
						 driver.findElement(By.xpath(".//*[@id='dwfrm_profileselection_otherteacher-1']")).click();
						 

						String continueToSite = driver.findElement(By.xpath(".//*[@id='profileSelection']/div/button")).getText();

						pw.write(continueToSite +"\t" +  "\n");
					
						driver.findElement(By.xpath(".//*[@id='profileSelection']/div[6]/button")).click();
						Thread.sleep(1000);
						driver.findElement(By.xpath(".//*[@id='dialog-container']/div/span")).click();
						Thread.sleep(2000);
						// MyAccount
						driver.findElement(By.xpath(".//*[@id='wrapper']/div[3]/div[1]/div[1]/div/div/ul/li[3]/a/span[3]")).click();
						driver.findElement(By.xpath(".//*[@id='wrapper']/div[3]/div[1]/div[1]/div/div/ul/li[3]/div/ul/li[5]/a")).click();

					}

					driver.close();
					driver.quit();
		
				}

				catch (Exception ex) {
					System.out.println("in While" + ex.getMessage());
					driver.close();
					driver.quit();
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			pw.flush();
			pw.close();
			driver.close();
			driver.quit();

		}

	}
}

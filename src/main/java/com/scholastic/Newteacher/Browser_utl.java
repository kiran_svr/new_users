package com.scholastic.Newteacher;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Browser_utl {

	public static WebDriver getSpecificDriver(String browerType) {
		WebDriver driver = null;;
		if (browerType.equals("IE")) {

			System.setProperty("webdriver.ie.driver", "server/IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
			caps.setCapability(
					InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
					true);
		}

		if (browerType.equals("chrome")) {

			DesiredCapabilities caps = DesiredCapabilities.chrome();
			System.setProperty("webdriver.chrome.driver", "server/chromedriver.exe");
			driver = new ChromeDriver();
		}

		if (browerType.equals("Firefox")) {
			System.setProperty("webdriver.gecko.driver", "server/geckodriver.exe");
			driver = new FirefoxDriver();
		}

		return driver;

	}

}

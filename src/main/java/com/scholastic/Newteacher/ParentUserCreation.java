package com.scholastic.Newteacher;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class ParentUserCreation {
	WebDriver driver;
	
	By ConnectToTeacher=By.xpath("//a[@id='parentConnect']");
	By ClassCode=By.xpath("//input[@id='dwfrm_connectteacher_classcode']");
	By Connect_CAC=By.xpath("//button[@class='button-continue']");
	By CreateAnAccount=By.xpath("//button[@class='button-continue']");
	By Fname=By.xpath("//input[@id='dwfrm_registration_firstname']");
	By Lname=By.xpath("//input[@id='dwfrm_registration_lastname']");
	By email=By.xpath("//input[@id='dwfrm_registration_email']");
	By pwd=By.xpath("//input[@id='dwfrm_registration_login_password']");
	By chkBox_Terms=By.xpath("//input[@id='dwfrm_registration_termsofuse']");
	By chkBox_Privacy=By.xpath("//input[@id='dwfrm_registration_privacy']");
	By Continue=By.xpath("//button[@class='button-continue']");
	
	By Child_Fname=By.xpath("//input[@id='dwfrm_addchild_childfirstname']");
	By Child_Lname=By.xpath("//input[@id='dwfrm_addchild_childlastname']");
	By Grade_select=By.xpath("(//div[@class='custom-select'])[1]");
	By Kindergarten=By.xpath("//span[text()='Kindergarten']");
	By Add=By.xpath("//button[@class='button-continue']");
	By GetStarted=By.xpath("//button[@class='button-continue']");
	
	//By AskMeLater=By.xpath("//div[@class='ask-me']");
	By Close_new=By.xpath("//div[@class='dialog-close icon-close-sm']");
	By Close_old=By.xpath("//a[@class='close']");
	By My_Account=By.xpath("//span[text()='My Account']");
	By SignOut=By.xpath("//a[@class='user-logout bold']");
	
	
	

	@Test
	public void User_Creation() throws InterruptedException{
	
	WebDriver driver=new FirefoxDriver();
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	driver.manage().window().maximize();
	//driver.get("https://clubs3qa1.scholastic.com/home");
	driver.get("https://storefront:cliff0rd@clubs-shingle.scholastic.com/home");
	
	String[] str = { "asd","Q4FTX","Q4FTY","Q4FTZ", "Q4FV2", "Q4FV3", "Q4FV6", "Q4FV8", "Q4FV9", "Q4FVB", "Q4FVC", "Q4FVD", "Q4FVF", "Q4FVG", "Q4FVH", "Q4FVJ", "Q4FVK", "Q4FVM", "Q4FVP", "Q4FVR", "Q4FVT", "Q4FVV", "Q4FVX", "Q4FVY", "Q4FW2", "Q4FW3", "Q4FW4", "Q4FW8", "Q4FW9", "Q4FWB", "Q4FWC", "Q4FWD", "Q4FWF", "Q4FWH", "Q4FWJ", "Q4FWK", "Q4FWL", "Q4FWM", "Q4FWN", "Q4FWQ", "Q4FWR", "Q4FWV", "Q4FWW", "Q4FWY", "Q4FWZ", "Q4FX2", "Q4FX3", "Q4FX4", "Q4FX6", "Q4FX7", "Q4FX8", "Q4FX9", "Q4FXB", "Q4FXC", "Q4FXD", "Q4FXF", "Q4FXG", "Q4FXJ", "Q4FXK", "Q4FXL", "Q4FXN", "Q4FXP", "Q4FXQ", "Q4FXT", "Q4FXV", "Q4FXW", "Q4FXZ", "Q4FY2", "Q4FY3", "Q4FY4", "Q4FY7", "Q4FY8", "Q4FY9", "Q4FYD", "Q4FYF", "Q4FYH", "Q4FYJ", "Q4FYK", "Q4FYL", "Q4FYM", "Q4FYN", "Q4FYP", "Q4FYR", "Q4FYT", "Q4FYV", "Q4FYW", "Q4FYX", "Q4FYZ", "Q4FZ2", "Q4FZ4", "Q4FZ6", "Q4FZ8", "Q4FZ9", "Q4FZB", "Q4FZC", "Q4FZF", "Q4FZG", "Q4FZJ", "Q4FZK", "Q4FZL", "Q4FZN"};
	for (int i = 1; i <= str.length; i++) {
		String val=str[i];
		
	String FirstName="Dwperfpt";
	String LastName="Lwperfpt";
	String password="password1";
	String Child_FirstName="Cwperfpa";
	String Child_LastName="Nwperfpa";
	
	Thread.sleep(2000);
	driver.findElement(ConnectToTeacher).click();
	Thread.sleep(2000);
	driver.findElement(ClassCode).click();
	driver.findElement(ClassCode).clear();
	driver.findElement(ClassCode).sendKeys(val);
	System.out.println(val);
	Thread.sleep(2000);
	driver.findElement(Connect_CAC).click();
	Thread.sleep(2000);
	driver.findElement(CreateAnAccount).click();
	Thread.sleep(2000);
	
	driver.findElement(Fname).click();
	driver.findElement(Fname).clear();
	driver.findElement(Fname).sendKeys(FirstName+i);
	
	Thread.sleep(2000);
	driver.findElement(Lname).click();
	driver.findElement(Lname).clear();
	driver.findElement(Lname).sendKeys(LastName+i);
	
	Thread.sleep(2000);
	driver.findElement(email).click();
	driver.findElement(email).clear();
	driver.findElement(email).sendKeys("dwperfpt"+i+"@scholastic.com");
	System.out.println("dwperfpt"+i+"@scholastic.com");
	
	Thread.sleep(2000);
	driver.findElement(pwd).click();
	driver.findElement(pwd).clear();
	driver.findElement(pwd).sendKeys(password);
	
	Thread.sleep(2000);
	driver.findElement(chkBox_Terms).click();
	driver.findElement(chkBox_Privacy).click();
	Thread.sleep(1000);
	driver.findElement(Continue).click();
	
	Thread.sleep(2000);
	driver.findElement(Child_Fname).click();
	driver.findElement(Child_Fname).clear();
	driver.findElement(Child_Fname).sendKeys(Child_FirstName+i);
	
	Thread.sleep(2000);
	driver.findElement(Child_Lname).click();
	driver.findElement(Child_Lname).clear();
	driver.findElement(Child_Lname).sendKeys(Child_LastName+i);
	
	Thread.sleep(2000);
	driver.findElement(Grade_select).click();
	Thread.sleep(1000);
	driver.findElement(Kindergarten).click();
	Thread.sleep(2000);
	driver.findElement(Add).click();
	Thread.sleep(3000);
	driver.findElement(GetStarted).click();
	Thread.sleep(7000);
	
	/*driver.findElement(Close_new).click();
	Thread.sleep(3000);*/
	driver.findElement(Close_old).click();
	Thread.sleep(7000);
	driver.findElement(My_Account).click();
	Thread.sleep(2000);
	driver.findElement(SignOut).click();
	Thread.sleep(7000);
	
	}
	}

}

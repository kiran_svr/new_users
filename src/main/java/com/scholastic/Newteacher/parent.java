package com.scholastic.Newteacher;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class parent {
	WebDriver driver;
	//tst comments
	By ConnectToTeacher=By.xpath("//a[@id='parentConnect']");
	By ClassCode=By.xpath("//input[@id='dwfrm_connectteacher_classcode']");
	By Connect_CAC=By.xpath("//button[@class='button-continue']");
	By CreateAnAccount=By.xpath("//button[@class='button-continue']");
	By Fname=By.xpath("//input[@id='dwfrm_registration_firstname']");
	By Lname=By.xpath("//input[@id='dwfrm_registration_lastname']");
	By email=By.xpath("//input[@id='dwfrm_registration_email']");
	By pwd=By.xpath("//input[@id='dwfrm_registration_login_password']");
	By chkBox_Terms=By.xpath("//input[@id='dwfrm_registration_termsofuse']");
	By chkBox_Privacy=By.xpath("//input[@id='dwfrm_registration_privacy']");
	By Continue=By.xpath("//button[@class='button-continue']");
	
	By Child_Fname=By.xpath("//input[@id='dwfrm_addchild_childfirstname']");
	By Child_Lname=By.xpath("//input[@id='dwfrm_addchild_childlastname']");
	By Grade_select=By.xpath("(//div[@class='custom-select'])[1]");
	By Kindergarten=By.xpath("//span[text()='Kindergarten']");
	By Add=By.xpath("//button[@class='button-continue']");
	By GetStarted=By.xpath("//button[@class='button-continue']");
	
	//By AskMeLater=By.xpath("//div[@class='ask-me']");
	By Close_new=By.xpath("//div[@class='dialog-close icon-close-sm']");
	By Close_old=By.xpath("//a[@class='close']");
	By My_Account=By.xpath("//span[text()='My Account']");
	By SignOut=By.xpath("//a[@class='user-logout bold']");
	
	
	

	@Test
	public void User_Creation() throws InterruptedException{
		
		 driver =Browser_utl.getSpecificDriver("chrome");
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	driver.manage().window().maximize();
	driver.get("https://clubs3qa1.scholastic.com/");
	
	for (int i = 21; i <= 60; i++) {
		
	String FirstName="dwr";
	String LastName="dwcool";
	String password="passw0rd";
	String Child_FirstName="Cwperf";
	String Child_LastName="Nwperf";
	
	try{
	Thread.sleep(2000);
	driver.findElement(ConnectToTeacher).click();
	Thread.sleep(2000);
	driver.findElement(ClassCode).click();
	driver.findElement(ClassCode).clear();
	driver.findElement(ClassCode).sendKeys("MD3BT");
	Thread.sleep(2000);
	driver.findElement(Connect_CAC).click();
	Thread.sleep(2000);
	driver.findElement(CreateAnAccount).click();
	Thread.sleep(2000);
	
	driver.findElement(Fname).click();
	driver.findElement(Fname).clear();
	driver.findElement(Fname).sendKeys(FirstName+i);
	
	Thread.sleep(2000);
	driver.findElement(Lname).click();
	driver.findElement(Lname).clear();
	driver.findElement(Lname).sendKeys(LastName+i);
	
	Thread.sleep(2000);
	driver.findElement(email).click();
	driver.findElement(email).clear();
	driver.findElement(email).sendKeys("dwrcool"+i+"@scholastic.com");
	
	Thread.sleep(2000);
	driver.findElement(pwd).click();
	driver.findElement(pwd).clear();
	driver.findElement(pwd).sendKeys(password);
	
	Thread.sleep(2000);
	driver.findElement(chkBox_Terms).click();
	driver.findElement(chkBox_Privacy).click();
	Thread.sleep(1000);
	driver.findElement(Continue).click();
	
	Thread.sleep(2000);
	driver.findElement(Child_Fname).click();
	driver.findElement(Child_Fname).clear();
	driver.findElement(Child_Fname).sendKeys(Child_FirstName+i);
	
	Thread.sleep(2000);
	driver.findElement(Child_Lname).click();
	driver.findElement(Child_Lname).clear();
	driver.findElement(Child_Lname).sendKeys(Child_LastName+i);
	
	Thread.sleep(2000);
	driver.findElement(Grade_select).click();
	Thread.sleep(1000);
	driver.findElement(Kindergarten).click();
	Thread.sleep(2000);
	driver.findElement(Add).click();
	Thread.sleep(3000);
	driver.findElement(GetStarted).click();
	Thread.sleep(7000);
	
	System.out.println("dwrcool"+i+"@scholastic.com");
	
	/*driver.findElement(Close_new).click();
	Thread.sleep(3000);*/
	driver.findElement(Close_old).click();
	Thread.sleep(7000);
	driver.findElement(My_Account).click();
	Thread.sleep(2000);
	driver.findElement(SignOut).click();
	Thread.sleep(7000);
	
	}
	catch(Exception e){
		driver.get("https://clubs3qa1.scholastic.com/");
		
	}
	}
	}

}
